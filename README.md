# Devuan Script
*Совместим с Devuan 4 - Chimaera*  
*В скрипт добавлены описания того что именно делает каждая строка*

## Использование:  
Распаковываем скрипт правым щелчком мыши, "Извлечь в текущую папку"  
Открываем devuan-script-master и devuan-script

Открываем сначала install-run-as-root.sh а потом scriptdev2.sh и стираем строки которые нам не нужны

![](https://notabug.org/eyeV/devuan-script/raw/master/Wallpapers/Sources/Edit.GIF)

Кликаем правой кнопкой мыши (ПКМ) на файл "install-run-as-root.sh"  
Выбираем свойства этого файла и во вкладке права включаем "Разрешить выполнение файла как программы"  
Закрываем свойства и кликаем ПКМ возле файлов в пустое место

В открывшемся контекстном меню выбираем "Открыть в терминале"  
Пишем в терминале:  
> su

Вводим пароль root (суперпользователя)  
Далее приступаем к запуску скрипта, пишем:  
> ./in

Где ./ это команда запуска скрипта а in это начало названия скрипта  
Нажимаем Tab и заполняется название скрипта  
В итоге мы видим чтото подобное:  
> root@PC:/home/devos/Загрузки/devuanscript# ./install-run-as-root.sh

Нажимаем Enter и ждем, скрипт разделен на две части

По окончании первой части скрипта вас попросят настроить меню запуска выбрав разрешение экрана, при этом нужно выбирать на пробел а подтверждать уже на Enter  
Если вы пропустили нажав на Enter не запускайте скрипт еще раз, запустите файл install.sh что находится в папке /grub/kawaiki-blight 

После установки первой части компьютер перезагрузится, после чего потребует ввести пароль root для запуска второй части скрипта

## Что делает первая часть скрипта:  
Сортирует репозитории  
Устанавливает простой скрипт обновления (Ярлык SysUpdate)  
Удаляет лишние приложения (Их можно будет установить в магазине приложений)  
Устанавливает kloak - сервис который мешает деанонимизации по набору текста  
Исправляет ошибку сети в среде gnome  
Создает шаблоны текстового файла, ярлыка и скрипта  
Устанавливает расширения DashToDoc, appindicator, desktop-icons  
Настраивает [тему grub](https://github.com/lucidtanooki/kawaiki-grub2-themes)  
Настраивает Центр Приложений так чтобы он работал

## Что делает вторая часть скрипта:  
Устанавливает:

Безопасный браузер - 	Librewolf  
Небезопасный браузер для случаев когда безопасный браузер не скачивает или не выгружает фото  
Парольные менеджеры - 	KeePass, Bitwarden  
Плеер для фильмов -  	VLC  
Аудио редакторы -    	FamiStudio, Zrythm, LMMS  
Видео редактор -     	Kdenlive  
Видео эффекты -      	Natron  
Видео конвертер -    	HandBrake  
3D моделирование -   	Blender  
Фоторедактор -       	Gimp  
Плагин -                PhotoGimp  
Рисование -          	Krita  
Очистка метадаты -   	Metadata Cleaner  
Торрент клиенты -    	qBittorrent, Tribler  
Архиватор -          	PeaZip  
Клавиатурный тренажер - Klavaro  
Редакторы тегов -    	EasyTAG, Ntag  
Редактор mkv тегов - 	MKVToolNixGUI  
Проверка хеша -      	GtkHash  
Аналог shazam -      	Mousai  
Запись экрана -      	OBS Studio  
Открытые игры -      	osu!, Minetest, Veloren  
Syncthing -             Синхронизация файлов  
QEMU -			        Менеджер виртуальных машин  
Обновляет стандартный архиватор  
Меняет настройки Gnome, настраивает док, список приложений, обои, муз. плеер, расширения и др.

## Доп. скрипты
Grub password          - устанавливает пароль для grub  
Repo-free/Repo-nonfree - меняет репозитории на свободные и не свободные