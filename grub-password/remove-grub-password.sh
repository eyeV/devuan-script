#Удаление уже установленных grub паролей

#Удаляет строки с паролем из 40_custom
sed -i '/password_pbkdf2 root grub./d'  /etc/grub.d/40_custom
sed -i '/set superusers=root/d' 	/etc/grub.d/40_custom

#Удаляет строки с паролем из бэкапа 40_custom.old (да, это нужно)
sed -i '/password_pbkdf2 root grub./d'  /etc/grub.d/40_custom.old
sed -i '/set superusers=root/d' 	/etc/grub.d/40_custom.old

#Удаляет строки с паролем из grub.cfg
sed -i '/password_pbkdf2 root grub.pbkdf2./d'  /boot/grub/grub.cfg
sed -i '/set superusers=root/d' 	/boot/grub/grub.cfg

#Убирает опцию --unrestricted из опций загрузки ядра
sed -i 's/CLASS="--class gnu-linux --class gnu --class os --unrestricted"/CLASS="--class gnu-linux --class gnu --class os"/g' /etc/grub.d/10_linux
sed -i 's/CLASS="--class gnu-linux --class gnu --class os --class xen --unrestricted"/CLASS="--class gnu-linux --class gnu --class os --class xen"/g' /etc/grub.d/34_linux_xen

sudo grub-mkconfig -o /boot/grub/grub.cfg
sudo update-grub
