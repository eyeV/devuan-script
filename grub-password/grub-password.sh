#Обновление репозиториев и установка зависимостей:
apt update
apt install -y sudo grub-common
####################################################################################################################
#Удаление дубликатов паролей (между решетками копия скрипта remove-grub-password.sh)

#Удаляет строки с паролем из 40_custom
sed -i '/password_pbkdf2 root grub./d'  /etc/grub.d/40_custom
sed -i '/set superusers=root/d' 	/etc/grub.d/40_custom

#Удаляет строки с паролем из бэкапа 40_custom.old (да, это нужно)
sed -i '/password_pbkdf2 root grub./d'  /etc/grub.d/40_custom.old
sed -i '/set superusers=root/d' 	/etc/grub.d/40_custom.old

#Удаляет строки с паролем из grub.cfg
sed -i '/password_pbkdf2 root grub.pbkdf2./d'  /boot/grub/grub.cfg
sed -i '/set superusers=root/d' 	/boot/grub/grub.cfg

#Убирает опцию --unrestricted из опций загрузки ядра
sed -i 's/CLASS="--class gnu-linux --class gnu --class os --unrestricted"/CLASS="--class gnu-linux --class gnu --class os"/g' /etc/grub.d/10_linux
#sed -i 's/CLASS="--class gnu-linux --class gnu --class os --class xen --unrestricted"/CLASS="--class gnu-linux --class gnu --class os --class xen"/g' /etc/grub.d/34_linux_xen

####################################################################################################################
#Команда что заставляет скрипт останавливатся при ошибке:
set -e

#Показывает данные инструкции в терминале
echo "Важно! Чтобы поставить пароль на Grub:
1) Откройте текстовый редактор комбинацией клавиш Win+E
2) Введите пароль которым зашифрован ваш devuan
3) Скопируйте его и вставте в поле ниже на Ctrl+Shift+V, или ПКМ>вставить
4) Нажмите Enter, вставьте пароль еще раз и снова подтвердите на Enter:"
#Записывает вывод комманды создания пароля в файл grubpassword.txt
grub-mkpasswd-pbkdf2 > grubpassword.txt 

#Удаляет лишние слова из файла grubpassword.txt оставляя только пароль
sed -i -e '1,2d' grubpassword.txt
sed -i 's/Хэш PBKDF2 вашего пароля://g' grubpassword.txt
sed -i 's/ //g' grubpassword.txt

#Записывает grubpassword.txt в переменную $grubpassword421
grubpassword421=$(< grubpassword.txt)

#Делает бэкап файла 40_custom
cp /etc/grub.d/40_custom /etc/grub.d/40_custom.old

#Добавляет параметры пароля и к нему пароль который содержится в переменной $grubpassword421
echo "set superusers="root"
password_pbkdf2 root $grubpassword421" >> /etc/grub.d/40_custom

#Удаляет файл grubpassword.txt и очищает переменную $grubpassword421 от пароля
rm -r grubpassword.txt
grubpassword421=123

#Записывает кавычки в переменные $cav
cav='"'
cavsysvinit='"sysvinit'

#Если существует файл с параметрами 10_linux
if grep -q "class gnu-linux --class gnu --class os" /etc/grub.d/10_linux; then
#То добавляет параметр который разрешает запуск Devuan без пароля Grub:
#(при редактировании параметров grub его будет нужно ввести)
	sed -i 's/CLASS="--class gnu-linux --class gnu --class os"/CLASS="--class gnu-linux --class gnu --class os --unrestricted"/g' /etc/grub.d/10_linux
#И выводит данный текст:
	echo "Строка заменена на строку с параметром"
#В противном случае (если файл с параметрами не существует):
else
#Добавляет файл с параметром который разрешает запуск Devuan без пароля Grub:
	echo "CLASS=$cav--class gnu-linux --class gnu --class os --unrestricted$cav
SUPPORTED_INITS=$cavsysvinit:/lib/sysvinit/init systemd:/lib/systemd/systemd upstart:/sbin/upstart$cav" >> /etc/grub.d/10_linux
#И выводит данный текст:
	echo "Файл с параметром создан"
#Конец if else
fi

#Обновляет Grub применяя к нему новые параметры, в нашем случае пароль и запуск системы без ввода пароля
sudo grub-mkconfig -o /boot/grub/grub.cfg
sudo update-grub

#nano /etc/grub.d/40_custom
