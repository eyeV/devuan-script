#УСТАНОВКА ПОЛЕЗНЫХ И ВАЖНЫХ ПРИЛОЖЕНИЙ:

#Безопасный браузер - Librewolf
flatpak install -y flathub io.gitlab.librewolf-community

#Удаление устаревшей версии Firefox
apt remove      -y  firefox-esr
apt autoremove  -y

####################################################################################################################
#Syncthing - синхронизация файлов, в том числе парольного менеджера, источник https://apt.syncthing.net/

#Скачивание ключа подписи:
curl -s -o /usr/share/keyrings/syncthing-archive-keyring.gpg https://syncthing.net/release-key.gpg
#Добавление apt репозитория:
echo "deb [signed-by=/usr/share/keyrings/syncthing-archive-keyring.gpg] https://apt.syncthing.net/ syncthing stable" | tee /etc/apt/sources.list.d/syncthing.list
#Обновление репозиториев:
apt-get update
#Установка Syncthing:
apt-get -y install syncthing

#Actiona   - приложение для автоматизации (кликер) 
sudo apt install -y actiona

#Qemu      - менеджер виртуальных машин
apt install -y qemu
apt install -y virt-manager
#Запуск сервиса libvirtd и добавление его в автозагрузку (нужно для запуска qemu)
sudo service   libvirtd start
sudo chkconfig libvirtd on

#Обновление стандартного архиватора, (удаление apt версии и установка flatpak версии)
apt remove -y file-roller
flatpak install -y flathub org.gnome.FileRoller

####################################################################################################################
#Подготовка к копированию настроек приложений
#Создать папку для настроек приложений:
mkdir -p						/home/$USERNAME/.var/app
#Разрешение чтения и записи в папку
chmod -R u+rwx					   	/home/$USERNAME/.var
chmod -R o+rwx						/home/$USERNAME/.var

####################################################################################################################
#Плеер для фильмов -  VLC
flatpak install -y flathub org.videolan.VLC

#Настройки VLC:
#Создает папку для текстового файла настроек видео плеера
mkdir -p 	  /home/$USERNAME/.var/app/org.videolan.VLC/config/vlc

#Добавляет текстовый файл настроек с размером окна
echo "geometry=@ByteArray(\x1\xd9\xd0\xcb\0\x3\0\0\0\0\x1\x95\0\0\0\xd2\0\0\x5\xee\0\0\x3\x96\0\0\x1\x95\0\0\0\xd2\0\0\x5\xee\0\0\x3\x96\0\0\0\0\0\0\0\0\a\x80\0\0\x1\x95\0\0\0\xd2\0\0\x5\xee\0\0\x3\x96)" >> /home/$USERNAME/.var/app/org.videolan.VLC/config/vlc/vlc-qt-interface.conf

#Добавляет текстовый файл настроек со следующим:
echo "
#Отключение подгона окна к размеру видео (стандартная настройка иногда скрывает док при открытии видео)
qt-video-autoresize=0

#Отключение показа названия при открытии видео (по умолчанию при открытии видео появляется большой текст с названием видео файла)
video-title-show=0
" >> /home/$USERNAME/.var/app/org.videolan.VLC/config/vlc/vlcrc

#Дает разрешение на чтение и запись этих файлов настроек
chmod -R o+rwx 	  /home/$USERNAME/.var/app/org.videolan.VLC/config/vlc
chmod -R u+rwx    /home/$USERNAME/.var/app/org.videolan.VLC/config/vlc

####################################################################################################################
#Плеер для музыки - QuodLibet
flatpak install -y flathub io.github.quodlibet.QuodLibet

#Копирование настроек музыкального плеера (убирает тонну лишнего и оставляет минимализм)
cp    -r appsettings/io.github.quodlibet.QuodLibet 	/home/$USERNAME/.var/app/

#Дает разрешение на чтение и запись этих файлов настроек
chmod -R u+rwx					   	/home/$USERNAME/.var/app/io.github.quodlibet.QuodLibet
chmod -R o+rwx					   	/home/$USERNAME/.var/app/io.github.quodlibet.QuodLibet

####################################################################################################################
#Просмотрщик фото с оригинальной цветовой схемой - qView
flatpak install -y flathub com.interversehq.qView

#Создает папку для текстового файла настроек qView
mkdir -p 	  /home/$USERNAME/.var/app/com.interversehq.qView/config/qView

#Добавляет текстовый файл настроек, которые: 
#Отключают подгон окна к размеру фото
#Делают размер окна примерно FullHD по умолчанию
#Отключают уведомления про обновления, так как для этого есть центр приложений
echo "[General]
firstlaunch=true
geometry=@ByteArray(\x1\xd9\xd0\xcb\0\x3\0\0\0\0\0\xfb\0\0\0^\0\0\x6\xb8\0\0\x3\xbf\0\0\0\xfb\0\0\0\x83\0\0\x6\xb8\0\0\x3\xbf\0\0\0\0\0\0\0\0\a\x80\0\0\0\xfb\0\0\0\x83\0\0\x6\xb8\0\0\x3\xbf)
optionsgeometry=@ByteArray(\x1\xd9\xd0\xcb\0\x3\0\0\0\0\x2\xc8\0\0\0\xd3\0\0\x5\xa5\0\0\x3U\0\0\x2\xc8\0\0\0\xf8\0\0\x5\xa5\0\0\x3U\0\0\0\0\0\0\0\0\a\x80\0\0\x2\xc8\0\0\0\xf8\0\0\x5\xa5\0\0\x3U)

[options]
windowresizemode=0" >> /home/$USERNAME/.var/app/com.interversehq.qView/config/qView/qView.conf

#Дает разрешение на чтение и запись этих файлов настроек
chmod -R o+rwx 	  /home/$USERNAME/.var/app/com.interversehq.qView
chmod -R u+rwx    /home/$USERNAME/.var/app/com.interversehq.qView

####################################################################################################################
#Скопировать стандартные обои devuan
cp -r Wallpapers/"Devuan OS Violet WL.png" /usr/share/backgrounds/gnome/
#Разрешения на чтение и запись:
chmod -R u+rw	/usr/share/backgrounds/gnome/"Devuan OS Violet WL.png"
chmod -R o+rw	/usr/share/backgrounds/gnome/"Devuan OS Violet WL.png"
#Переименовать файл обоев "Devuan OS Violet WL" в "DevWL":
mv /usr/share/backgrounds/gnome/"Devuan OS Violet WL.png" /usr/share/backgrounds/gnome/DevWL
####################################################################################################################
#Удаление ярлыка скрипта из папки автозагрузки:
rm -r /etc/xdg/autostart/scriptdev2.desktop
#Удаление скрипта devos (он запускает вторую часть скрипта, тоесть этот скрипт)
rm -r /home/$USERNAME/.local/share/devos.sh

####################################################################################################################
#УСТАНОВКА ПОЛЕЗНЫХ И ВАЖНЫХ ПРИЛОЖЕНИЙ ИЗ FLATHUB:

#Небезопасный браузер для случаев когда безопасный браузер не скачивает или не загружает фото, или не открывает страницы
flatpak install -y com.github.Eloston.UngoogledChromium

#Парольные менеджеры - KeePass, Bitwarden
flatpak install -y flathub com.bitwarden.desktop
flatpak install -y flathub org.keepassxc.KeePassXC

#Аудио редакторы -    FamiStudio, Zrythm, LMMS
flatpak install -y flathub org.famistudio.FamiStudio
flatpak install -y flathub org.zrythm.Zrythm
flatpak install -y flathub io.lmms.LMMS

#Видео редактор -     Kdenlive
flatpak install -y flathub org.kde.kdenlive

#Видео эффекты -      Natron
flatpak install -y flathub fr.natron.Natron

#Видео конвертер -    HandBrake
flatpak install -y flathub fr.handbrake.ghb

#3D моделирование -   Blender
flatpak install -y flathub org.blender.Blender

#Фоторедактор -       Gimp
flatpak install -y flathub org.gimp.GIMP

#Плагин фотошоп -     PhotoGimp, источник https://github.com/Diolinux/PhotoGIMP
#От имени пользователя:
su $USER -c'
#Переход в домашнюю папку:
cd /home/$USERNAME/
#Скачивание плагина из официального GitHub репозитория: 
git clone https://github.com/Diolinux/PhotoGIMP.git
#Разрешение на запуск, чтение и запись плагина:
chmod -R u+rwx	PhotoGIMP
#Копирование плагина по соответствующим папкам:
cp -r 		PhotoGIMP/.git 			/home/$USERNAME/
cp -r 		PhotoGIMP/.local 		/home/$USERNAME/
cp -r 		PhotoGIMP/.var 			/home/$USERNAME/
cp -r 		PhotoGIMP/.gitignore 		/home/$USERNAME/
cp -r 		PhotoGIMP/.editorconfig 	/home/$USERNAME/
#Удаление скачанных файлов:
rm -r 		PhotoGIMP'

#Рисование -          Krita
flatpak install -y flathub org.kde.krita

#Очистка метадаты -   Metadata Cleaner
flatpak install -y flathub fr.romainvigier.MetadataCleaner

#Торрент клиенты -    qBittorrent, Tribler
flatpak install -y flathub org.qbittorrent.qBittorrent
flatpak install -y flathub org.tribler.Tribler

#Архиватор -          PeaZip
flatpak install -y flathub io.github.peazip.PeaZip

#Клавиатурный тренажер -  Klavaro
flatpak install -y flathub net.sourceforge.Klavaro

#Редакторы тегов -    EasyTAG, Ntag
flatpak install -y flathub org.gnome.EasyTAG
flatpak install -y flathub com.github.nrittsti.NTag

#Редактор mkv тегов - MKVToolNixGUI
flatpak install -y flathub org.bunkus.mkvtoolnix-gui

#Инструмент для слияния и сравнения текстовых файлов - Diffuse
flatpak install -y flathub io.github.mightycreak.Diffuse

#Менеджер расширений Gnome - Extension Manager
flatpak install -y flathub com.mattjakeman.ExtensionManager

#Аналог shazam -      Mousai
flatpak install -y flathub io.github.seadve.Mousai

#Запись экрана -      OBS Studio
flatpak install -y flathub com.obsproject.Studio

#Открытые игры -      osu!, Minetest, Veloren
flatpak install -y flathub sh.ppy.osu
flatpak install -y flathub net.minetest.Minetest
flatpak install -y flathub net.veloren.veloren

####################################################################################################################
#Применение настроек gnome второй раз:

#Копирование текстового файла настроек gnome в домашнюю папку:
cp -r extsettings/saved_settings.dconf /home/$USER/
#Разрешения на чтение и запись:
chmod -R u+rwx 			       /home/$USER/saved_settings.dconf
chmod -R o+rwx 			       /home/$USER/saved_settings.dconf

#От имени пользователя:
su $USER -c'
#Переход в домашнюю папку:
cd /home/$USER/
#Применение настроек из скопированного текстового файла:
dconf load / < saved_settings.dconf
#Удаление скопированного текстового файла
rm -r /home/$USER/saved_settings.dconf'

####################################################################################################################
#Повторное удаление ярлыка на devos из папки автозагрузки(на всякий случай):
rm -r /etc/xdg/autostart/scriptdev2.desktop
#Повторное удаление скрипта devos (того самого что после перезагрузки запускает вторую часть скрипта, тоесть этот скрипт который вы сейчас читаете)
rm -r /home/$USERNAME/.local/share/devos.sh

#Прислать уведомление об успешном завершении выполнения скрипта:
su $USER -c'notify-send -u critical "Выполнение скрипта завершено"'

#Конец
#Скрипт создан eyeV
