####################################################################################################################
#Применение настроек gnome для того чтобы предотвратить уход компьютера в сон во время выполнения скрипта:

#Копирование текстового файла настройки отключения сна в домашнюю папку, и его переименование:
cp -r extsettings/screenlockoff /home/$USER/saved_settings.dconf
#Разрешения на чтение, запись и запуск текстового файла:
chmod -R u+rwx 			       /home/$USER/saved_settings.dconf
chmod -R o+rwx 			       /home/$USER/saved_settings.dconf

#От имени пользователя:
su $USER -c'
#Переход в домашнюю папку:
cd /home/$USER/
#Применение настроек из скопированного текстового файла:
dconf load / < saved_settings.dconf
#Удаление скопированного текстового файла
rm -r /home/$USER/saved_settings.dconf'
####################################################################################################################
#Сортировка репозиториев:

#Удаляет стандартный файл с репозиториями:
rm -r /etc/apt/sources.list

#Добавляет отсортированные репозитории, источник - https://www.devuan.org/os/packages
echo "#Devuan 4.0 Chimaera (stable)
deb     http://deb.devuan.org/merged chimaera           main
deb     http://deb.devuan.org/merged chimaera-updates   main
deb     http://deb.devuan.org/merged chimaera-security  main
deb-src http://deb.devuan.org/merged chimaera           main
deb-src http://deb.devuan.org/merged chimaera-updates   main
deb-src http://deb.devuan.org/merged chimaera-security  main

#Devuan 3.1   Beowulf (Для зависимостей)
deb 	http://deb.devuan.org/merged beowulf          	main
deb-src http://deb.devuan.org/merged beowulf          	main

#Devuan 2.1   ASCII   (Для зависимостей)
deb 	http://deb.devuan.org/merged ascii          	main
deb-src http://deb.devuan.org/merged ascii          	main

#Devuan 1.0.0 Jessie  (Для зависимостей)
deb 	http://archive.devuan.org/merged jessie         main
deb-src http://archive.devuan.org/merged jessie         main

#NON FREE (НЕ СВОБОДНЫЕ, ВКЛЮЧАТЬ ТОЛЬКО ЕСЛИ ВСЕ ЧТО ВЫШЕ ЗАКОММЕНТИРОВАНЫ)
#deb      http://deb.devuan.org/merged chimaera          main contrib non-free
#deb-src  http://deb.devuan.org/merged chimaera          main contrib non-free
#deb 	  http://deb.devuan.org/merged beowulf           main contrib non-free
#deb-src  http://deb.devuan.org/merged beowulf           main contrib non-free
#deb 	  http://deb.devuan.org/merged ascii          	 main contrib non-free
#deb-src  http://deb.devuan.org/merged ascii          	 main contrib non-free
#deb 	  http://archive.devuan.org/merged jessie	 main contrib non-free
#deb-src  http://archive.devuan.org/merged jessie	 main contrib non-free


#Источник: https://www.devuan.org/os/packages
" >> /etc/apt/sources.list

#Обновление списка репозиториев:
apt update
####################################################################################################################
#Удаление лишних предустановленных игр и приложений (вы сможете установить их новые версии позже, из центра приложений)
apt remove -y gnome-online-miners gnome-gmail gnome-games gnome-games-app gnome-maps gnome-2048 gnome-chess gnome-klotski gnome-mahjongg gnome-nibbles gnome-robots gnome-sudoku gnome-taquin atomix atomix-data five-or-more tali iagno cheese transmission-gtk rhythmbox malcontent libgnome-todo gnome-todo gnome-todo-common gnome-contacts
#Очищение оставшихся от них зависимостей
apt autoremove -y

#Установка важных системных пакетов для дальнейшей работы скрипта:
apt install -y sudo libdbus-glib-1-2 pup wget git sed curl chkconfig libnotify-bin
####################################################################################################################
#Скрипт Обновления:

#Копирование папки со скриптом:
cp    -r system-update	   /usr/share/applications/

#Разрешения на запуск и чтение скрипта:
chmod -R u+rwx		   /usr/share/applications/system-update
chmod -R o+rx		   /usr/share/applications/system-update

####################################################################################################################
#Исправление ошибки сети в среде gnome (заменяет false на true):
sed -i  's/managed=false/managed=true/g' 	/etc/NetworkManager/NetworkManager.conf

#Перезапуск сетевого менеджера (выключено так как может вылететь сессия и скрипт остановится):
#sudo service network-manager restart
####################################################################################################################
#Установка работающих расширений (док, иконки раб. стола, индикатор фон. приложений и другое):
apt install -y gnome-shell-extension-dashtodock gnome-shell-extension-desktop-icons gnome-shell-extension-no-annoyance gnome-shell-extension-panel-osd gnome-shell-extension-appindicator gnome-shell-extension-hide-activities gnome-shell-extension-autohidetopbar
#Установка расширения проверки хеша для файлового менеджера:
apt install -y nautilus-gtkhash
#Установка расширения бесследного удаления файлов для файлового менеджера:
apt install -y nautilus-wipe

#Копирование настроек расширений (настройки дока, и др.):

#Удаляет предыдущую версию, копирует настройки дока:
rm -r /usr/share/gnome-shell/extensions/dash-to-dock@micxgx.gmail.com
cp -r extsettings/dash-to-dock@micxgx.gmail.com 	/usr/share/gnome-shell/extensions/

#Удаляет предыдущую версию, копирует настройки иконок рабочего стола:
rm -r /usr/share/gnome-shell/extensions/desktop-icons@csoriano
cp -r extsettings/desktop-icons@csoriano 		/usr/share/gnome-shell/extensions/

#Удаляет предыдущую версию, копирует настройки "отключения уведомления при открытии копии приложения":
rm -r /usr/share/gnome-shell/extensions/noannoyance@sindex.com
cp -r extsettings/noannoyance@sindex.com 		/usr/share/gnome-shell/extensions/

#Удаляет предыдущую версию, копирует настройки перемещения уведомления в правый нижний угол (расширение отключено по умолчанию):
rm -r /usr/share/gnome-shell/extensions/panel-osd@berend.de.schouwer.gmail.com
cp -r extsettings/panel-osd@berend.de.schouwer.gmail.com /usr/share/gnome-shell/extensions/


#Разрешения доступа к скопированным настройкам расширений:
chmod -R u+rwx	/usr/share/gnome-shell/extensions/panel-osd@berend.de.schouwer.gmail.com
chmod -R o+rx	/usr/share/gnome-shell/extensions/panel-osd@berend.de.schouwer.gmail.com

chmod -R u+rwx	/usr/share/gnome-shell/extensions/noannoyance@sindex.com
chmod -R o+rx	/usr/share/gnome-shell/extensions/noannoyance@sindex.com

chmod -R u+rwx	/usr/share/gnome-shell/extensions/desktop-icons@csoriano
chmod -R o+rx	/usr/share/gnome-shell/extensions/desktop-icons@csoriano

chmod -R u+rwx	/usr/share/gnome-shell/extensions/dash-to-dock@micxgx.gmail.com
chmod -R o+rx	/usr/share/gnome-shell/extensions/dash-to-dock@micxgx.gmail.com
####################################################################################################################
#Скопировать редактируемые обои в папку "Изображения", и дать им разрешения на чтение:
cp -r Wallpapers/Sources 			/home/$USER/Изображения/
#Разрешение на чтение и запись скопированных редактируемых обоев:
chmod -R u+rwx					/home/$USER/Изображения/Sources
chmod -R o+rwx					/home/$USER/Изображения/Sources


#Создать папку для стандартных обоев
mkdir -p					/home/$USER/.local/share/backgrounds

#Скопировать стандартные обои devuan в папку "Изображения", также они появлятся в меню Gnome
cp    -r Wallpapers/"Devuan OS Violet NL.png" 	/home/$USER/.local/share/backgrounds/
cp    -r Wallpapers/"Devuan OS Violet WL.png" 	/home/$USER/.local/share/backgrounds/
cp    -r Wallpapers/"Devuan OS Green.png" 	/home/$USER/.local/share/backgrounds/

#Разрешение на чтение и запись папки обоев
chmod -R u+rw					/home/$USER/.local/share/backgrounds
chmod -R o+rw					/home/$USER/.local/share/backgrounds
####################################################################################################################
#Скопировать текстовый файл с командами терминала в папку "Документы"
su $USER -c'cp -r "Команды терминала" /home/$USER/Документы/'

#Добавление шаблона текстового файла в папку "Шаблоны"
su $USER -c'touch /home/$USER/Шаблоны/"Новый документ"'

#Добавление шаблона ярлыка в папку "Шаблоны"
su $USER -c'echo "[Desktop Entry]
Type=Application
Name=
Icon=/home/$USERNAME/.local/share/applications/icons/
Exec=/home/$USERNAME/.local/share/applications/apps/
Terminal=false
" >> /home/$USER/Шаблоны/F2.desktop'

#Добавление шаблона скрипта в папку "Шаблоны"
su $USER -c'echo "#!/bin/bash


" >> /home/$USER/Шаблоны/Script.sh'
####################################################################################################################
#Установка Магазина Flathub для центра приложений, источник: https://flatpak.org/setup/Debian
apt install -y flatpak
apt install -y gnome-software-plugin-flatpak
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
####################################################################################################################
#Установка сервиса который мешает деанонимизации по набору текста - Kloak, источник https://github.com/vmonaco/kloak

#Выполнение комманды от имени текущего пользователя:
su $USERNAME -c'
#Переход в домашнюю дерикторию:
cd /home/$USERNAME/
#Скачивание ключа подписи Whonix - разработчика Kloak:
wget https://www.whonix.org/patrick.asc'

#Выполнение комманды в новом ответвлении root:
su root -c'
#Переход в домашнюю дерикторию:
cd /home/$USERNAME/
#Проверка ключа подписи Whonix:
sudo apt-key --keyring /etc/apt/trusted.gpg.d/whonix.gpg add /home/$USER/patrick.asc'

#Добавление apt репозитория Whonix - разработчика Kloak:
echo "deb https://deb.whonix.org bullseye main" | sudo tee /etc/apt/sources.list.d/whonix.list

#Обновление репозиториев:
sudo apt update
#Установка сервиса kloak:
sudo apt install -y kloak

#Удаление использованного ключа подписи Whonix:
rm -r /home/$USERNAME/patrick.asc
####################################################################################################################
#Установка темы оформления Grub

#Установка зависимостей
apt install -y grub-customizer plymouth plymouth-themes plymouth-x11 dialog

#Удаление стандартного фона grub (без этого невозможно будет сменить фон)
rm -r /boot/grub/splash.png

#Скачивание темы grub из github, источник - https://github.com/lucidtanooki/kawaiki-grub2-themes
#Эта тема оформления автоматически ставит разрешение grub таким как разрешение вашего экрана
git clone https://github.com/lucidtanooki/kawaiki-grub2-themes.git

#Удаление предыдущей версии темы grub (если она там есть)
rm -r grub-theme/kawaiki-grub2-themes
#Перемещение скачанной темы в папку grub-theme
mv kawaiki-grub2-themes grub-theme

#Удаление папки backgrounds с обоями
rm -r grub-theme/kawaiki-grub2-themes/backgrounds
#Копирование вместо них обоев "blight-background"
cp -r grub-theme/blight-background 			 	 grub-theme/kawaiki-grub2-themes
#Переименование blight-background в backgrounds
mv    grub-theme/kawaiki-grub2-themes/blight-background		 grub-theme/kawaiki-grub2-themes/backgrounds

#Разрешение на чтение и запуск скачанных тем
chmod -R o+rwx grub-theme/kawaiki-grub2-themes
chmod -R u+rwx grub-theme/kawaiki-grub2-themes

#От имени пользователя
su $USER -c'
#Присылает данное уведомление
notify-send -u critical "НЕ НАЖИМАЙТЕ ПОКА ENTER. Делайте выбор на пробел."'

#Меняет папку установки темы на ту которая поддерживается в devuan
sed -i  's|THEME_DIR="/usr/share/grub/themes"|THEME_DIR="/boot/grub/themes"|g' grub-theme/kawaiki-grub2-themes/install.sh

#Запускает установщик темы оформления grub
./grub-theme/kawaiki-grub2-themes/install.sh

#Обновляет параметры и настройки Grub
sudo update-grub2
sudo update-initramfs -u
####################################################################################################################
#Прописывает в автозагрузку запуск второй части скрипта

#В переменную $patttssth записывается текущий путь который показывает команда pwd:
patttssth=`pwd`
#В переменную $cav записывается кавычка:
cav='"'

#Записывает в папку share данный скрипт, назовем его devos, он запустит вторую часть скрипта после перезагрузки:
echo "#Показывает данное сообщение о вводе пароля root:
echo $cavВведите пароль суперпользователя (root) для продолжения исполнения скрипта:$cav

#Комманда которая от имени рута запускает вторую часть скрипта используя ранее записанные переменные
#Также эта комманда продублирована много раз на случай неправильного ввода пароля root, знаки || запустят следующую копию комманды если первая не сработает, если сработает то не запустит
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'||
su root -c'cd $cav$patttssth$cav; ./scriptdev2.sh'" >> /home/$USERNAME/.local/share/devos.sh

#Дает разрешения на запуск данного скрипта:
chmod -R o+rwx /home/$USERNAME/.local/share/devos.sh
chmod -R u+rwx /home/$USERNAME/.local/share/devos.sh
#Дает разрешения на запуск второй части скрипта:
chmod -R o+rwx scriptdev2.sh
chmod -R u+rwx scriptdev2.sh

#Добавляет ярлык скрипта devos в папку автозагрузки:
echo "[Desktop Entry]
Type=Application
Name=Scriptdev2 autostart
Exec=/home/$USERNAME/.local/share/devos.sh
Terminal=true
NoDisplay=false
X-GNOME-Autostart-enabled=true
X-KDE-autostart-after=panel" >> /etc/xdg/autostart/scriptdev2.desktop
#Дает разрешение на чтение данного ярлыка:
chmod -R o+rwx /etc/xdg/autostart/scriptdev2.desktop
chmod -R u+rwx /etc/xdg/autostart/scriptdev2.desktop

#Перезагружает систему:
sudo reboot
####################################################################################################################
